@extends('layouts.app')

@section('titre', 'Accueil ')

@section('content')


    <div class="flex centered">
        <h1>- Tous les films -</h1>
    </div>

        <div class="row padding-top">


            @foreach($films as $film)


                <!-- <div class="col s12 m7 l4"> -->


            <div id="index-card">

                <div class="col s12 m6 l3 flex centered">

                    <div class="card large">

                        <div class="card-image">

                            <img src="img/film/{{ $film->image }}">

                        </div>
                        <div class="card-content">
                            <span class="card-title">
                                <li>{{ $film->titre }} {{ $film->etoiles }}</li></span>
                            <p><li>{{ $film->synopsis }}</li></p>
                        </div>
                        <div class="card-action">
                          <form method="post">
                            <select id="film-etoiles-{{ $film->id }}" class="etoiles">
                              <option value="1" {{ $film->etoiles == '1' ? 'selected' : '' }}>{{ $film->id }}</option>
                              <option value="2" {{ $film->etoiles == '2' ? 'selected' : '' }}>{{ $film->id }}</option>
                              <option value="3" {{ $film->etoiles == '3' ? 'selected' : '' }}>{{ $film->id }}</option>
                              <option value="4" {{ $film->etoiles == '4' ? 'selected' : '' }}>{{ $film->id }}</option>
                              <option value="5" {{ $film->etoiles == '5' ? 'selected' : '' }}>{{ $film->id }}</option>
                            </select>
    </form>
                            <a class="waves-effect waves-light btn grey lighten-1 right"{{ link_to_route('film.show', 'Détail', $film->id) }}</a>
                    </div>
                </div>





        </div>




    </div>






            @endforeach


        </div>

@endsection
