@extends('layouts.app')

@section('titre', 'Ajout-film')

@section('content')


    <h1>Ajouter un film</h1>

    <div id="create-film" class="row">

        <div class="col s12 m6 l6">

            {!! Form::open([$film, 'action' => 'FilmController@store', 'files' => 'true']) !!}

            {!! Form::label('titre','Titre: ') !!}<br>
            {!! Form::text('titre') !!}<br>

            {!! Form::label('annee','Année de production: ') !!}<br>
            {!! Form::text('annee') !!}<br>

            {!! Form::label('image','Image:') !!}<br>
            {!! Form::file('image') !!}

            {!! Form::label('id_classement','Classement:') !!} <br>

            {!!  Form::select('id_classement', $classements) !!}
 
            {!! Form::label('duree','Durée: ') !!}<br>
            {!! Form::text('duree') !!}<br>

        </div>

        <div class="col s12 m6 l6">

            {!! Form::label('synopsis','Synopsis: ') !!}<br>
            {!! Form::textarea('synopsis') !!}<br>

            {!! Form::label('acteurs','Acteurs: ') !!}<br>
            {!! Form::text('acteurs') !!}<br>

            {!! Form::submit('Ajouter', array('class' => 'btn bouton-submit-create left')) !!}

            {!! Form::close() !!}

            @if (count($errors) > 0)
                <ul style="color:#ff1744">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

        </div>

    </div>

@stop
