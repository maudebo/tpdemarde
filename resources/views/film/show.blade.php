@extends('layouts.app')

@section('titre', 'Details-Film')

@section('content')


    <div class="padding-top">

        <div class="blanc">

            <div class="row">

                <img src="../img/film/{{ $film->image }}" class="col s12 m6 l4" alt="Image film">

                <div class="card-action row" id="carte-detail-film">

                        <div class="row flex align-item-center">

                            <div id="boutons-modif" class="row">

                                <h2 id="titre-detail" class="col">{{ $film->titre }}</h2>


                              @if (auth()->check())
                                @if (auth()->user()->isAdmin())

                                {{ Form::open(array('url' => 'film/' . $film->id, 'class' => 'col pull-left')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Supprimer', array('class' => 'btn bouton-rouge')) }}
                                {!! Form::close() !!}

                                {{ Form::open(['url' => 'film/'.$film->id.'/edit', 'class' => 'col pull-left', 'method' => 'GET']) }}

                                {{ Form::submit('Modifier', array('class' => 'btn bouton-rouge')) }}
                                {!! Form::close() !!}
                                @endif
                              @endif
                            </div>

                        </div> {{-- bloc titre + boutons --}}

                        <div class="col s12 m6 l4">

                            <p class=""><span>Synopsis:</span></p>

                            <p class="synopsis">{{ $film->synopsis }}</p>

                        </div> {{-- bloc SYNOPIS --}}

                        <div class="col s12 m6 l3">

                            <p><span>Durée : </span> {{ $film->duree }} minutes</p>


                            <p><span>Acteurs : </span><br> {{ $film->acteurs }}</p>

                            <p><span>Année : </span> {{ $film->annee }}</p>

                            <p><span>Classement : </span> {{ $film->classement->nom }}</p>

                            <p><span class="col sans-margin">Cote des membres :</span></p>

                            <p><form method="post" class="">

                                <select id="film-etoiles" class="etoiles">

                                    <option value="1" {{ $film->etoiles == '1' ? 'selected' : '' }}>{{ $film->id }}</option>
                                    <option value="2" {{ $film->etoiles == '2' ? 'selected' : '' }}>{{ $film->id }}</option>
                                    <option value="3" {{ $film->etoiles == '3' ? 'selected' : '' }}>{{ $film->id }}</option>
                                    <option value="4" {{ $film->etoiles == '4' ? 'selected' : '' }}>{{ $film->id }}</option>
                                    <option value="5" {{ $film->etoiles == '5' ? 'selected' : '' }}>{{ $film->id }}</option>

                                </select>

                            </form></p>

                            <p><span>Nombre total de vote :</span></p>

                        </div> {{-- bloc affiche acteur, annee, nb vote --}}

                </div>

            </div>

        </div>

    </div>


{{-- critique current user  --------------------------------------------------------------------------------------}}


@if(isset($idUserCritiques) && Auth::check())

    @foreach($idUserCritiques as $idUserCritique)

        @if($idUserCritique == Auth::user()->id)

            <?php $critiqueUser = true; ?> {{-- Quelle syntaxe remplace le php Laravel dans ce cas-ci? --}}

            @break

        @endif

    @endforeach

@endif




@if(!isset($critiqueUser) && Auth::check())

    <div class="ma-critique">

        <h4 id="h4">Ajouter une critique</h4>

        {!! Form::open(['route' => 'critique.store', 'files' => 'true']) !!}

        {!! Form::label('Vote','Vote: ') !!}

        <select id="film-etoiles-{{ $film->id }}" class="etoiles">

            <option value="1" {{ $film->etoiles == '1' ? 'selected' : '' }}>{{ $film->id }}</option>
            <option value="2" {{ $film->etoiles == '2' ? 'selected' : '' }}>{{ $film->id }}</option>
            <option value="3" {{ $film->etoiles == '3' ? 'selected' : '' }}>{{ $film->id }}</option>
            <option value="4" {{ $film->etoiles == '4' ? 'selected' : '' }}>{{ $film->id }}</option>
            <option value="5" {{ $film->etoiles == '5' ? 'selected' : '' }}>{{ $film->id }}</option>

        </select>

        {!! Form::label('commentaire','Commentaire: ') !!}
        {!! Form::textarea('commentaire') !!}
        {!! Form::hidden('id_film', $film->id) !!}
        {!! Form::submit('Ajouter une critique', array('class' => 'btn btn-warning')) !!}
        {!! Form::close() !!}

        @if(isset($critique))

            {{ Form::open(array('url' => 'critique/' . $critique->id, 'class' => 'pull-right')) }}
            {{ Form::hidden('_method', 'DELETE') }}
            {{ Form::submit('Supprimer', array('class' => 'btn btn-warning')) }}
            {{ Form::close() }}

        @endif


        {{-- bloc erreur   --------------------------------------}}

        @if (count($errors) > 0)

            <ul style="color:#ff1744">

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        @endif

        {{-- fin bloc erreur   ----------------------------------}}

    </div>

@endif


{{-- fin   --------------------------------------------------------------------------------------------------}}

    {{-- critique de Auth ------------------------------------------------------------------------------------------------}}

    @if(count($critiques) != 0 && isset($film))

                @foreach($film->critiques as $critique)

                    @if(Auth::check() && Auth::user()->id == $critique->id_utilisateur)

                        @if(app('request')->input('allo')!= null && app('request')->input('allo')==404)

                            <div class="ma-critique">

                                <div class="row">

                                    <h4 id="h4" class="col h3-critique">Ma critique</h4>

                                    <p class="col date"> ajoutée le {{ date('d F Y', strtotime($critique->created_at)) }}</p>

                                </div>

                                {!! Form::model($critique, ['method' => 'PATCH', 'files' => 'true', 'route' => ['critique.update', $critique->id]]) !!}

                                {!! Form::label('Vote','Vote: ') !!}

                                <select id="film-etoiles-{{ $film->id }}" class="etoiles">

                                    <option value="1" {{ $film->etoiles == '1' ? 'selected' : '' }}>{{ $film->id }}</option>
                                    <option value="2" {{ $film->etoiles == '2' ? 'selected' : '' }}>{{ $film->id }}</option>
                                    <option value="3" {{ $film->etoiles == '3' ? 'selected' : '' }}>{{ $film->id }}</option>
                                    <option value="4" {{ $film->etoiles == '4' ? 'selected' : '' }}>{{ $film->id }}</option>
                                    <option value="5" {{ $film->etoiles == '5' ? 'selected' : '' }}>{{ $film->id }}</option>

                                </select>

                                {!! Form::label('commentaire','Commentaire: ') !!}
                                {!! Form::textarea('commentaire') !!}
                                {!! Form::hidden('id_film', $film->id) !!}
                                {!! Form::submit('Enregistrer', array('class' => 'btn')) !!}
                                {!! Form::close() !!}

                            </div>

                        @else

                            <div class="ma-critique">

                                <div class="row flex align-item-center">

                                    <h4 id="h4" class="col h3-critique">Ma critique</h4>

                                    <p class="col date"> ajoutée le {{ date('d F Y', strtotime($critique->created_at)) }}</p>

                                    <a class="col btn left" href="{{route('film.show', $film->id.'?'.'id='.$critique->id.'&'.'allo=404') }}">Modifier</a>

                                </div>

                                <p class="synopsis">{{ $critique->commentaire }}</p>

                            </div>

                        @endif

                    @endif

                @endforeach

                {{-- fin critique de Auth --------------------------------------------------------------------------}}

                <div id="user-critique">

                @if(count($critiques) != 0 && Auth::check())

                    <h4 id="h4">Critiques des membres</h4>

                    @foreach($critiques as $critique)

                        @if(Auth::user()->id != $critique->id_utilisateur)

                            <div class="row">

                                <h5 class=" col">Critique ajouée par {{ $critique->user->login }}</h5>

                                <h5 class="col"> {{ date('d F Y', strtotime($critique->created_at)) }} </h5>

                                <span class="ligne"></span>

                            </div>

                            <p>{{ $critique->commentaire }}</p>

                        @endif

                    @endforeach

                @endif

         @endif


                        @if(count($critiques) != 0 && !Auth::check())

                            <h4 id="h4">Critiques des membres</h4>

                            @foreach($critiques as $critique)

                                    <div class="row">

                                        <h5 class="col">Critique ajouée par {{ $critique->user->login }}</h5>

                                        <h5 class="col"> {{ date('d F Y', strtotime($critique->created_at)) }} </h5>

                                    </div>

                                    <p>{{ $critique->commentaire }}</p>

                            @endforeach

                        @endif

                    </div>


    @endsection
