<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use App\Http\Requests;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class ProfileController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'login' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
     public function showModifierCompte()
     {
         // show the form
         return View::make('auth.modifier-compte');
     }


     public static function update(Request $request)
     {
       $rules = array(
           'email'    => 'required|email', // make sure the email is an actual email
           'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
     );

   // run the validation rules on the inputs from the form
   $validator = Validator::make(Input::all(), $rules);

   // if the validator fails, redirect back to the form
   if ($validator->fails()) {
       return View::make('auth.modifier-compte')
           ->withErrors($validator) // send back all errors to the login form
           ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
   } else {

     $id_utilisateur = $request->user()->id;
     $user = User::find($id_utilisateur);
     $user->login = $request->input('login');
     $user->email = $request->input('email');
     $user->password = bcrypt($request->input('password'));

     $user-> update();
     return Redirect::to('film');
 }
}


    public function getLogout()
    {
        $this->auth->logout();
        Session::flush();
        return redirect('/');
    }
}
