<?php

namespace App\Http\Controllers;

use App\Classement;

use App\Film;
use App\Http\Controllers\CritiqueController;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Input;

use App\Critique;

use App\User;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\File;

use App\Http\Requests\CreateFilmRequest;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;


class FilmController extends Controller


{

    public function __construct()
    {
       //$this->middleware('auth'); // Ceci limite l'accès à la modif des film aux membres seulement.
    }

    // Create function -----------------------------------

    public function index()
    {

        $films = Film::all();
        $avecEtoiles = CritiqueController::filmsAvecEtoiles($films);
        return view('film.index')->withFilms($avecEtoiles);



        return view('film.index')->withFilms($film);
    }

    // Create function -----------------------------------

    public function create()
    {



        $classements = Classement::lists('nom','id');

        $film = new Film();

        return View::make('film.create', compact('film', 'classements'));

        //return  View::make('film.create', compact('classements', 'film'));
    }

    // Store function -----------------------------------

    public function store(CreateFilmRequest $request)
    {

        $donnees = $request->all();


        $image = $donnees['image'];

        //$image->resize(270,152, function ($constraint) {$constraint->aspectRatio();});

        //$image = Image::make($image)->resize(100, 100);

        $destinationPath = 'img/film';

        $extension = $image->getClientOriginalExtension();

        $nomImage = 'A-'.rand(11111, 99999) . '.' . $extension; // A- pour ajoutée

        $uploadReussi = $image->move($destinationPath, $nomImage);

        $film = new Film();
        $film->titre = $donnees['titre'];
        $film->annee = $donnees['annee'];
        $film->image = $nomImage;
        $film->id_classement = $donnees['id_classement'];
        $film->duree = $donnees['duree'];
        $film->synopsis = $donnees['synopsis'];
        $film->acteurs = $donnees['acteurs'];
        //$film->created_at => Carbon\Carbon::now()
        $film->save();

        //$films = Film::all();


        if ($uploadReussi)
        {

            flash()->success('Un nouveau film à été Ajouté!'); // success() ajoute une classe qui donne la couleur verte à la box message




            return Redirect::to('/');

        }

    }


    // Edit function -----------------------------------

    public function edit($id)
    {

        $film = Film::findOrFail($id);

        $classements = Classement::lists('nom', 'id');

        //$selected = $film->classement->id;

        return View::make('film.edit', compact('film', 'classements'));

    }

    public function update($id, CreateFilmRequest $request)
    {

        $donnees = $request->all();

        $film = Film::findOrFail($id);

       if($request->hasFile('image')) {

           $oldimage = $film->image;

           $destinationPath = 'img/film';

           $image = $donnees['image'];

           $extension = $image->getClientOriginalExtension();

           $nomImage = 'M-'.rand(11111, 99999) . '.' . $extension;  // M- pour modifiée

           //$image = Image::make($image)->resize(100, 100)->encode('jpg');

           $image->move($destinationPath, $nomImage, 75);

           $film->image = $nomImage;

           File::delete($destinationPath. '/' .$oldimage);
       }


        $film->titre = $donnees['titre'];
        $film->annee = $donnees['annee'];
        $film->id_classement = $donnees['id_classement'];
        $film->duree = $donnees['duree'];
        $film->synopsis = $donnees['synopsis'];
        $film->acteurs = $donnees['acteurs'];
        //$film->created_at => Carbon\Carbon::now()






        $film->save(); // J'ai essayé la fonction update() mais elle ne semble pas fonctionner: $film->update($request->all());, donc j'utilise simplement la fonction save()

        flash()->success('Le film à été modifié!'); // success() = une classe qui donne la couleur verte à la box du message

        return Redirect::to('/');


    }

    // Show function -----------------------------------

   public function show($id)
    {
        $film = Film::findOrFail($id);
        $etoiles = CritiqueController::nbEtoiles($id);
        $film['etoiles'] = $etoiles->moyenne;



        $critiques  = Critique::where('id_film', $id)->orderBy('created_at', 'desc')->get();

        $idUserCritiques = Critique::select('id_utilisateur')->where('id_film', $id)->distinct()->pluck('id_utilisateur');

        return view('film.show', compact('film', 'critiques', 'idUserCritiques'));
    }

     public function destroy($id)
    {
        $film = Film::findOrFail($id);

        $critiques = Critique::where('id_film', $id)->delete();

        $oldimage = $film->image;

        $film->delete();

        $destinationPath = 'img/film';

        File::delete($destinationPath. '/' .$oldimage);

        flash('Le film à été supprimé'); // success() ajoute une classe qui donne la couleur verte à la box message

        return Redirect::to('/');
    }
}
