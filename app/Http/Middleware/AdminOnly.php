<?php

namespace App\Http\Middleware;

use Closure;

class AdminOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
      {
          if (\Auth::user()->is_admin == 2)
          {
              return $next($request);
          }

          return redirect()->guest('/');
      }
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
