<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{


    public function critiques()
    {
        return $this->hasMany('App\Critique', 'id_utilisateur');
    }



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'in_roles', 'password', 'remember_token',
    ];

    public function role()
    {
      return $this->belongsTo('App\Role');
    }

    /**
     * Check if user has admin role.
     *
     * @return mixed
     */
     public function isAdmin()
   {
    //print_r( $this->roles());
    //return true;
    return $this->id_roles == 2;

    //return $this->hasRole('membre');
   }

}
